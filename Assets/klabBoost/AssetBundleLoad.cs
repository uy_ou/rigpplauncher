﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using klab.iO;

namespace klab.iO
{
	public class AssetBundleLoad : MonoBehaviour
	{
		AssetBundle assetBundle;

		public GameObject textureGo,audioGo;
		TextureToByte textureToByte = new TextureToByte();
		RawImage raw;
		Texture2D texture2d;

		AudioSource audioSource;
		AudioClip audioClip;

		// klab
		// string filePath = "http://www.klab.com/jp/cms/wp-content/uploads/2009/01/klab_icon_200901.jpg";

		// twitter
		// string filePath = "https://pbs.twimg.com/profile_images/597349883062800384/4uh5WCYA_400x400.jpg";

		// soundcloud jpg
		// string filePath = "https://i1.sndcdn.com/avatars-000136408719-mejcgd-t200x200.jpg";

		// free audio
		string filePath = "http://www.ne.jp/asahi/music/myuu/wave/loop1.wav";

		// drive miss
		// string filePath = "https://lh6.googleusercontent.com/T6sh8ij3YLsuQLBRXLAn-Dz1VF4rncVtc307OPZa1CNfbHX49ERwacsX8CmJDKJAtH1OBneQFwdNXhA=w1340-h563-rw";

		bool io = false;

		void Start()
		{
			// texture = GetComponent<GUITexture>().texture;
			// texture2d = new Texture2D(256,256);
			// raw = textureGo.GetComponent<RawImage>();
			// textureToByte.entryPoint(filePath);

			audioSource = audioGo.GetComponent<AudioSource>();
			textureToByte.entryPoint(filePath);
		}

		public void test()
		{
			if(textureToByte.isDone && !io)
			{
				Debug.Log("1");
				if(texture2d.LoadImage(textureToByte.getBytes()))
				{
					Debug.Log("3");
					raw.texture = texture2d;
					io = true;
				}
			}
			else {
				Debug.Log("2");
				raw.texture = new Texture2D(256,256);
				io = false;
			}

		}

		public void testAudio()
		{
			if(textureToByte.isDone)
			{
				Debug.Log("3");
				float[] f = ConvertByteToFloat(textureToByte.getBytes());
				Debug.Log("f.Length = " + f.Length);
				audioClip = AudioClip.Create("testSound", f.Length, 1, 44100, false, false);
    			audioClip.SetData(f, 0);
				audioSource.clip = audioClip;
				audioSource.Play();
			}
			else {
				Debug.Log("2");
			}
		}

		private float[] ConvertByteToFloat(byte[] array)
        {
			float[] floatArr = new float[array.Length / 4];
			for (int i = 0; i < floatArr.Length; i++)
			{
				if (BitConverter.IsLittleEndian)
				Array.Reverse(array, i * 4, 4);
				floatArr[i] =
					BitConverter.ToSingle(array, i*4) / 0x80000000;
			}
			return floatArr;
		}
	}
}