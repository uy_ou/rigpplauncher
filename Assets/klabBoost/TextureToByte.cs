﻿//using UnityEngine;
using System;
using System.Net;
using System.IO;
using System.Text;

using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace klab.iO
{
	public class TextureToByte{
		//受信した全データを入れておくMemoryStream
	    private System.IO.MemoryStream requestData;
	    //受信したデータが一時的に入るバイト型配列
	    private  byte[] bufferData;

	    public bool isDone = false;
	    private byte[] bytes;

	    public byte[] getBytes()
	    {
	    	return bytes;
	    }

	    //エントリポイント
	    public void entryPoint(string filePath)
	    {
			//Https用 SSL認証エラー回避
			ServicePointManager.ServerCertificateValidationCallback =
				new RemoteCertificateValidationCallback(OnRemoteCertificateValidationCallback);

			//HttpWebRequestを作成
			HttpWebRequest webreq =
				(HttpWebRequest)WebRequest.Create(filePath);

	        //非同期要求を開始
	        //状態オブジェクトとしてHttpWebRequestを渡す
	        IAsyncResult r = (IAsyncResult)webreq.BeginGetResponse(
	            new AsyncCallback(ResponseCallback), webreq);
	    }

	    //非同期要求が終了した時に呼び出されるコールバックメソッド
	    private void ResponseCallback(IAsyncResult ar)
	    {
	        //状態オブジェクトとして渡されたHttpWebRequestを取得
	        HttpWebRequest webreq =
	        	(HttpWebRequest)ar.AsyncState;

	        //非同期要求を終了
	        HttpWebResponse webres =
	        	(HttpWebResponse)webreq.EndGetResponse(ar);

	        //データを読み込むためのストリームを取得
	        System.IO.Stream st = webres.GetResponseStream();

	        //データを読み込むための準備をする
	        requestData = new System.IO.MemoryStream();
	        bufferData = new byte[1024];

	        //非同期でデータの読み込みを開始
	        //状態オブジェクトとしてStreamを渡す
	        IAsyncResult r = (IAsyncResult)st.BeginRead(
	            bufferData, 0, bufferData.Length,
	            new AsyncCallback(ReadCallback), st);
	    }

	    //非同期読み込み完了時に呼び出されるコールバックメソッド
	    private void ReadCallback(IAsyncResult ar)
	    {
	        //状態オブジェクトとして渡されたStreamを取得
	        System.IO.Stream st = (System.IO.Stream)ar.AsyncState;

	        //データを読み込む
	        int readSize = st.EndRead(ar);

	        //データが読み込めたか調べる
	        if (readSize > 0)
	        {
	            //データが読み込めた時
	            //読み込んだデータをMemoryStreamに保存する
	            requestData.Write(bufferData, 0, readSize);

	            //再び非同期でデータを読み込む
	            IAsyncResult r = (IAsyncResult)st.BeginRead(
	                bufferData, 0, bufferData.Length,
	                new AsyncCallback(ReadCallback), st);
	        }
	        else
	        {
	            //データの読み込みが終了した時
	            //ダウンロードしたデータをバイト型配列に変換
	            byte[] sourceData = requestData.ToArray();
	            //データを文字列に変換
	            string sourceHtml =
	                System.Text.Encoding.UTF8.GetString(sourceData);

	                isDone = true;

	                bytes = requestData.ToArray();
	                //閉じる
	            st.Close();
	            requestData.Close();
	        }
		}


		// 信頼できないSSL証明書を「問題なし」にするメソッド
	    private bool OnRemoteCertificateValidationCallback(
	      Object sender,
	      X509Certificate certificate,
	      X509Chain chain,
	      SslPolicyErrors sslPolicyErrors)
	    {
	        return true;  // 「SSL証明書の使用は問題なし」と示す
	    }
	}
}