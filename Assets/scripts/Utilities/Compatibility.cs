﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;

namespace RiG_GameLanucher.Utilities
{
	///	<summary>
	///	互換性を保つ時に必要なものをまとめたクラス
	///	</summary>
	/// <example> 
	///	参照するときはこれを使いたい
	///	using static RiG_GameLanucher.Utilities.Compatibility;
	///	しかし,できなかったので暫定的に
	///	using COMP = RiG_GameLanucher.Utilities.Compatibility;
	/// </example>
	public static class Compatibility
	{
		//System.OperatingSystem os = System.Environment.OSVersion;
		
		//static string PRT = os.Platform.ToString();
		public static readonly char SP = Path.DirectorySeparatorChar;
	}
}