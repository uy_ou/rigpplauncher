﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.IO;
using System.Collections;
using myExplorer = RiG_GameLanucher.Utilities.Explorer.myExplorer;
using COMP = RiG_GameLanucher.Utilities.Compatibility;

namespace RiG_GameLanucher.Utilities.Explorer
{
	public class myFile : MonoBehaviour, IPointerClickHandler {
		public string path;
		public string dir;
		public Text target;
		char SP = COMP.SP;

		void Update(){
			target.text = path;
		}

		public void OnPointerClick (PointerEventData eventData){
			if( eventData.clickCount == 1 ){
				SingleClick();
			}
			else if(eventData.clickCount > 1){
				DoubleClick();
			}
		}

		public void SingleClick(){
			string toPath = dir + SP + path;
			Debug.Log("Dir : "+ dir);
			Debug.Log("Path : "+ path);
			Debug.Log("toPath : " + toPath);
		}

		public void DoubleClick(){
			string toPath = dir + SP + path;
			if(File.Exists(toPath))
			{
				myExplorer.choosePath(toPath);
			}
			else if(Directory.Exists(toPath))
			{
				toPath += SP;
				myExplorer.moveToPath(toPath);
			}
			else
			{
				return;
			}
		}
	}
}