﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.IO;
using System.Collections;
using myExplorer = RiG_GameLanucher.Utilities.Explorer.myExplorer;
using COMP = RiG_GameLanucher.Utilities.Compatibility;

namespace RiG_GameLanucher.Utilities.Explorer
{
	public class myPath : MonoBehaviour
	{
		public string path;
		public string dir;
		public Text target;
		char SP = COMP.SP;

		void Update()
		{
			target.text = path;
		}

		public void OnClick()
		{
			Debug.Log("Dir :" + dir);
			Debug.Log("path :" + path);

			string toPath = dir + path + SP;
			myExplorer.moveToPath(toPath);
		}
	}
}