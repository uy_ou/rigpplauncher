﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Collections;
using UnityEngine.EventSystems;
using COMP = RiG_GameLanucher.Utilities.Compatibility;

namespace RiG_GameLanucher.Utilities.Explorer
{
	public class myExplorer : MonoBehaviour {
		public static string pathLog;
		public static string selectedFile;
		public Text Title;
		public Text resultPath;
		public GameObject pathT;
		public GameObject fileT;
		public GameObject pathPrefab;
		public GameObject filePrefab;
		static bool status = false;

		// public enum FilePathType
		// {
		// 	NotFound,
		// 	File,
		// 	Directory,
		// }

		// public static FilePathType GetFilePathType(string path)
		// {
		// 	if(File.Exists(path)) return FilePathType.File;
		// 	if(Directory.Exists(path)) return FilePathType.Directory;
		// 	return FilePathType.NotFound;
		// }

		static char SP = COMP.SP;

		void Start(){
			Title.text = "フォルダを選択してください";
			pathLog = Environment.CurrentDirectory;
			selectedFile = pathLog;
			this.init();
		}

		void Update(){
			resultPath.text = selectedFile;
			if(status)
			{
				clear();
				init();
				status = false;
			}
		} 

		public static void moveToPath(string path){
			pathLog = path.TrimEnd(SP);
			selectedFile = pathLog;
			Debug.Log("MoveTo :" + pathLog);
			status = true;
		}

		public static void choosePath(string path){
			selectedFile = path;
		}

		public void onOK(){
			Debug.Log("OK");
			Debug.Log(resultPath.text);
		}

		public void onCancel(){
			Debug.Log("Cancel");

			this.clear();
		}

		public void init(){
			GameObject clone;
			// pathlog
			string prePath = "";
			foreach (string SpPath in pathLog.Split(SP)) {
				clone = Instantiate(pathPrefab) as GameObject;
				RectTransform rectTrans = clone.GetComponent<RectTransform>();
				rectTrans.parent = pathT.GetComponent<RectTransform>();
				rectTrans.localScale = new Vector3(1,1,1);
		
				var myPath = clone.GetComponent<myPath>();
				myPath.dir = prePath;
				myPath.path = SpPath;
				prePath += SpPath + SP;
    		}

			foreach (string SpDir in Directory.GetDirectories(pathLog+SP)) {
				clone = Instantiate(filePrefab) as GameObject;
				RectTransform rectTrans = clone.GetComponent<RectTransform>();
				rectTrans.parent = fileT.GetComponent<RectTransform>();
				rectTrans.localScale = new Vector3(1,1,1);
				
				var myFile = clone.GetComponent<myFile>();
				myFile.dir = pathLog;
				myFile.path = SpDir.Replace(pathLog+SP,"");
    		}

    		foreach (string SpDir in Directory.GetFiles(pathLog+SP)) {
				clone = Instantiate(filePrefab) as GameObject;
				RectTransform rectTrans = clone.GetComponent<RectTransform>();
				rectTrans.parent = fileT.GetComponent<RectTransform>();
				rectTrans.localScale = new Vector3(1,1,1);
				
				var myFile = clone.GetComponent<myFile>();
				myFile.dir = pathLog;
				myFile.path = SpDir.Replace(pathLog+SP,"");
    		}
		}

		public void clear(){
			foreach ( RectTransform rectTrans in pathT.GetComponent<RectTransform>() )
			{
				GameObject.Destroy(rectTrans.gameObject);
			}
			foreach ( RectTransform rectTrans in fileT.GetComponent<RectTransform>() )
			{
				GameObject.Destroy(rectTrans.gameObject);
			}
			status = true;
		}

	}
}