﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RiG_GameLanucher.Utilities
{
	public class AudioManager : SingletonMonoBehaviour<AudioManager> 
	{
		private const string BGM_PATH = "DTMs/BGM"; // Assets/Resources/DTMs/BGM
		
		private AudioSource bgmSource = null;
		
		private Dictionary<string,AudioClip> bgmDict = null; // for sort
		private List<AudioClip> bgmList = new List<AudioClip>(); // for play
		private int bgmNumMax;
		private int bgmPlayingNum = 0;

		public void Awake () 
		{
			if (this != Instance)
			{
				Destroy(this);
				return;
			}
			DontDestroyOnLoad (this);

			// 準備は以下に書く
			// create AudioSources
			this.bgmSource = this.gameObject.AddComponent<AudioSource>();
			
			// assign BGM files from folder to list
			object[] bgmObject = Resources.LoadAll (BGM_PATH);
			
			// create clip dictionaries
			this.bgmDict = new Dictionary<string, AudioClip>();
			foreach(AudioClip bgm in bgmObject){
				bgmDict[bgm.name] = bgm;
			}
			// assign clips to BGM List to play
			foreach(AudioClip bgm_dicted in bgmDict.Values ){
				bgmList.Add(bgm_dicted);
			}
			bgmNumMax = bgmList.Count;
			//Debug.Log ("Max:" + bgmNumMax);
		}

		void Start()
		{
			// play initial BGM on Awake
			this.bgmSource.clip = this.bgmList [0];
			this.bgmSource.Play ();
		}

		void Update()
		{
			// loop in BGM_PATH
			if(!this.bgmSource.isPlaying){
				if(bgmPlayingNum + 1 == bgmNumMax){
					bgmPlayingNum = 0;
				}
				else{
					++bgmPlayingNum;
				}
				this.bgmSource.clip = this.bgmList[bgmPlayingNum];
				this.bgmSource.Play ();
			}
		}
		/*** 機能は以下の関数 ***/
		/*** TODO overloadで機能拡張必要***/

		public void PlayBGM() 
		{

		}

		public void StopBGM() 
		{

		}

		public void PlaySE() 
		{

		}

		public void StopSE() 
		{

		}
	}
}