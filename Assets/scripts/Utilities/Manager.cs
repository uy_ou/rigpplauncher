using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using RiG_GameLanucher.Model;
using RiG_GameLanucher.Utilities;
using COMP = RiG_GameLanucher.Utilities.Compatibility;
//using RiG_GameLanucher.Utilities.Compatibility;

namespace RiG_GameLanucher.Utilities
{
	public class Manager : SingletonMonoBehaviour<Manager> 
	{
		LauncherSettings ls = new LauncherSettings();
		//ContentConstants constants = new ContentConstants();
		//Content SelectContent;

		public void Awake () 
		{
			if (this != Instance)
			{
				Destroy(this);
				return;
			}
			DontDestroyOnLoad (this);

			// 準備は以下に書く
			
			init();
			
			// Audio準備

			// その他,Managerクラス作成
		}

		void init(){
			//	ランチャー設定データの読み込み
			using (var r = new XmlTextReader(new FileStream(".LAUNCHER_DATA" + COMP.SP + "SETTINGS" + COMP.SP + "settings.xml", FileMode.Open)))
			{
				XmlSerializer xs = new XmlSerializer(typeof(LauncherSettings));
				this.ls = (LauncherSettings)xs.Deserialize(r);
			}

			//	XMLファイルの読み取り
			//		Game
			string[] Gamefiles = Directory.GetFiles(Environment.CurrentDirectory,
				this.ls.GameInfoFileName, SearchOption.AllDirectories);

			foreach(string f in Gamefiles){
				ContentConstants.addContent<GameInfo>(new GameInfo(f));
			}
			
			//		DTM
			string[] DTMfiles = Directory.GetFiles(Environment.CurrentDirectory,
				this.ls.DTMInfoFileName, SearchOption.AllDirectories);

			foreach(string f in DTMfiles){
				ContentConstants.addContent<DTMInfo>(new DTMInfo(f));
			}
			
			//		DDGraphic
			string[] DDGraphicfiles = Directory.GetFiles(Environment.CurrentDirectory,
				this.ls.DDGraphicInfoFileName, SearchOption.AllDirectories);

			foreach(string f in DDGraphicfiles){
				ContentConstants.addContent<DDGraphicInfo>(new DDGraphicInfo(f));
			}
			
			//		DDDGraphic
			string[] DDDGraphicfiles = Directory.GetFiles(Environment.CurrentDirectory,
				this.ls.DDDGraphicInfoFileName, SearchOption.AllDirectories);

			foreach(string f in DDDGraphicfiles){
				ContentConstants.addContent<DDDGraphicInfo>(new DDDGraphicInfo(f));
			}
		}
	}
}