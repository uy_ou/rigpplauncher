﻿using UnityEngine;
using System.Collections;

namespace RiG_GameLanucher.Utilities
{
	public class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour 
	{
		private static T instance;
		public static T Instance  
		{
			get {
				if (instance == null){
					instance = (T)Object.FindObjectOfType(typeof(T));

					if (instance == null) 
					{
						Debug.LogError(typeof(T) + "is nothing");
					}
				}
				return instance;
			}
		}
	}
}