using System.Collections;
using System.Collections.Generic;
using RiG_GameLanucher.Model;

namespace RiG_GameLanucher.Model
{
	public static class ContentConstants {
		static List<Content> AllContentsList = new List<Content>();
		static List<GameInfo> AllGamesList = new List<GameInfo>();
		static List<DTMInfo> AllDTMsList = new List<DTMInfo>();
		static List<DDGraphicInfo> AllDDGraphicsList = new List<DDGraphicInfo>();
		static List<DDDGraphicInfo> AllDDDGraphicsList = new List<DDDGraphicInfo>();
		

		public static void addContent<T>(T newContent) where T : Content
		{
			AllContentsList.Add(newContent);

			if(newContent is GameInfo)
			{
				AllGamesList.Add(newContent as GameInfo);
			}
			else if(newContent is DTMInfo)
			{
				AllDTMsList.Add(newContent as DTMInfo);
			}
			else if(newContent is DDGraphicInfo)
			{
				AllDDGraphicsList.Add(newContent as DDGraphicInfo);
			}
			else if(newContent is DDDGraphicInfo)
			{
				AllDDDGraphicsList.Add(newContent as DDDGraphicInfo);
			}			
		}

		public static List<GameInfo> getGameList()
		{
			return AllGamesList;
		}

		public static List<DTMInfo> getDTMList()
		{
			return AllDTMsList;
		}

		public static List<DDGraphicInfo> getDDGraphicList()
		{
			return AllDDGraphicsList;
		}

		public static List<DDDGraphicInfo> getDDDGraphicList()
		{
			return AllDDDGraphicsList;
		}
	}
}