﻿using UnityEngine;
using System;
using System.IO;
using System.Xml;
using System.Collections.Specialized;

namespace RiG_GameLanucher.Model
{
	///	<<summary>>
	///	ゲーム情報管理クラス
	///	<<summary>>
	public class GameInfo : Content
	{
		//TODO	プロパティはsetのアクセシビリティの制限を強くしたい
		// IContents
		public override string Name {get; set;}
		public override string FilePath {get; set;}
		public override string Readme{get; set;}
		public override bool isCompletes{get; set;}

		// GameInfo
		public string ScreenShot { get; set; }	// スクリーンショットの絶対パス
		public string Genre { get; set; }		// ジャンル
		public int Level { get; set; }			// レベル（1-5）
		public int Player { get; set; }			// プレイ人数
		public int Pad { get; set; }			// パッド対応の有無

		public GameInfo()
		{
			this.Name = "hoge";
		}

		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="xmlPath">ゲーム情報XMLファイルへの絶対パス</param>
		public GameInfo(string xmlPath)
		{
			var dirPath = Path.GetDirectoryName(xmlPath);   // ディレクトリまでのパス

			// 設定ファイルのパース
			// rootノード以下のすべての要素をコレクションに追加
			var document = new XmlDocument();
			document.Load(xmlPath);
			var root = document.DocumentElement;
			var collection = new NameValueCollection();
			
			foreach (XmlNode elm in root.ChildNodes)
			{
				collection.Add(elm.Name, elm.InnerText);
			}

			// // 各情報を設定
			try
			{
				this.Name = collection["Name"];
				Debug.Log("dirPath : " + dirPath);
				Debug.Log("collection[FilePath] : " + collection[FilePath]);
				this.FilePath = Path.Combine(dirPath, collection["FilePath"]);
				this.Readme = Path.Combine(dirPath, collection["Readme"]);
				
			// 	this.Genre = collection["Genre"];
			// 	this.Level = int.Parse(collection["Level"]);
			// 	this.Player = int.Parse(collection["Player"]);
			// 	this.Pad = int.Parse(collection["Pad"]);
			// 	this.ScreenShot = Path.Combine(dirPath, collection["ScreenShot"]);
			}
			catch (Exception)
			{
				//Debug.Log("\"" + xmlPath + "\"の読み込みに失敗しました.\n設定ファイル読み込みエラー");
				throw;
			}
		}
	}
}