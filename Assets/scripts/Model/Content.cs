﻿using UnityEngine;
using System;
using System.Collections;

namespace RiG_GameLanucher.Model
{ 
	public abstract class Content : IContents
	{
		public abstract string Name {get; set;}
		public abstract string FilePath {get; set;}
		public abstract string Readme{get; set;}

		public abstract bool isCompletes{get;set;}
	}
}