﻿using UnityEngine;
using System;
using System.IO;
using System.Xml;
using System.Collections.Specialized;

namespace RiG_GameLanucher.Model
{
	public class DDDGraphicInfo : GraphicInfo
	{
		// IContents
		public override string Name {get; set;}
		public override string FilePath {get; set;}
		public override string Readme{get; set;}
		public override bool isCompletes{get; set;}

		// GraphicInfo
		

		public DDDGraphicInfo(){
			this.Name = null;
		}
		
		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="xmlPath">ゲーム情報XMLファイルへの絶対パス</param>
		public DDDGraphicInfo(string xmlPath){
			var dirPath = Path.GetDirectoryName(xmlPath);   // ディレクトリまでのパス

			// 設定ファイルのパース
			// rootノード以下のすべての要素をコレクションに追加
			var document = new XmlDocument();
			document.Load(xmlPath);
			var root = document.DocumentElement;
			var collection = new NameValueCollection();
			
			foreach (XmlNode elm in root.ChildNodes)
			{
				collection.Add(elm.Name, elm.InnerText);
			}

			// 各情報を設定
			try
			{
				this.Name = collection["Name"];
				this.FilePath = Path.Combine(dirPath, collection["FilePath"]);
				this.Readme = Path.Combine(dirPath, collection["Readme"]);
			}
			catch (Exception)
			{
				Debug.Log("\"" + xmlPath + "\"の読み込みに失敗しました.\n設定ファイル読み込みエラー");
				throw;
			}
		}
	}
}	