﻿using UnityEngine;
using System.Collections;

namespace RiG_GameLanucher.Model
{ 
	public interface IContents 
	{
		string Name {get; set;}
		string FilePath {get; set;}
		string Readme{get; set;}

		bool isCompletes{get;}
	}
}