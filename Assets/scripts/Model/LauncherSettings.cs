﻿using System;
using System.Collections;

namespace RiG_GameLanucher.Model
{
	public class LauncherSettings{
		public string GameInfoFileName;
		public string DTMInfoFileName;
		public string DDGraphicInfoFileName;
		public string DDDGraphicInfoFileName;
	}
}