﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using RiG_GameLanucher.Model;

namespace RiG_GameLanucher.UI.Panel
{
	public class DTMPanel : AbstractPanel 
	{
		public DTMInfo dtmInfo;

		void Start () 
		{
			init();
    	}

    	protected override void init()
    	{
			base.init();
		}

    	public override void setContent(Content content)
    	{
    		this.dtmInfo = content as DTMInfo;
    	}

    	public override void OnClick ()
		{
			Debug.Log("DTM Panel");
		}
	}
}