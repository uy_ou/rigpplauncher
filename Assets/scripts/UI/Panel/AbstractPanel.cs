using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Xml;
using RiG_GameLanucher.Model;

namespace RiG_GameLanucher.UI.Panel
{
	public abstract class AbstractPanel : MonoBehaviour {
		void Start()
		{

		}

		protected virtual void init()
		{
			Button button = GetComponent<Button>();
		    button.onClick.AddListener (() => {
		    	OnClick();
		    });
		}

		public abstract void setContent(Content content);
		public abstract void OnClick ();
	}
}