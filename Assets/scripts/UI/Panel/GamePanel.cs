using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using RiG_GameLanucher.Model;

namespace RiG_GameLanucher.UI.Panel
{
	public class GamePanel : AbstractPanel {
		public GameInfo gameInfo;

		void Start () 
		{
			init();
    	}

    	protected override void init()
    	{
			base.init();
		}

    	public override void setContent(Content content)
    	{
    		this.gameInfo = content as GameInfo;
    	}

		public override void OnClick ()
		{	
			//System.Diagnostics.Process p = System.Diagnostics.Process.Start(this.gameInfo.FilePath);
		}
	}
}