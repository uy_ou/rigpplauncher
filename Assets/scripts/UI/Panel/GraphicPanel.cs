﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using RiG_GameLanucher.Model;

namespace RiG_GameLanucher.UI.Panel
{
	public class GraphicPanel : AbstractPanel 
	{
		public GraphicInfo graphic;

		void Start () 
		{
	        init();
    	}

    	protected override void init()
    	{
			base.init();
		}

    	public override void setContent(Content content){

    	}

    	public override void OnClick ()
		{
			Debug.Log("Graphic Panel");
		}
	}
}