﻿using UnityEngine;
using System.Collections;

namespace RiG_GameLanucher.UI.Window
{
	public abstract class AbstractWindow : MonoBehaviour 
	{
		public GameObject ContentsFrame;

		protected abstract void init();
	}
}