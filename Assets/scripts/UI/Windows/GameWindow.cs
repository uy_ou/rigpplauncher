using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using RiG_GameLanucher.UI.Panel;
using RiG_GameLanucher.UI.ToolTip;
using RiG_GameLanucher.Model;
using Contents = RiG_GameLanucher.Model.ContentConstants;

namespace RiG_GameLanucher.UI.Window
{
	public class GameWindow : AbstractWindow 
	{

		// public GameObcjet ContentFrame;
		public GameObject GamePanelPrefab;
		public GameObject toolTip;

		void Start()
		{
			init();
			createGamePanel();
		}

		protected override void init()
		{
			//// gameボタン追加 
			// foreach(GameInfo game in ContentConstants.getGameList())
			// {
			// 	 ここでprefab作成
			// }
		}

		void createGamePanel()
		{
			GameObject gamePanel;
			foreach(GameInfo game in Contents.getGameList())
			{
				gamePanel = Instantiate(GamePanelPrefab) as GameObject;
				gamePanel.transform.parent = ContentsFrame.transform;
				gamePanel.transform.localScale = Vector3.one;

				GamePanel panel = gamePanel.transform.gameObject.GetComponent<GamePanel>();
				panel.gameInfo = game;

				ShowTooltip show = gamePanel.transform.gameObject.GetComponent<ShowTooltip>();
				show.toolTipObj = toolTip;
				show.info = game;

				//gamePanel.transform.GetChild(0).GetComponent<Image>().sprite = game.
				gamePanel.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = game.Name;
			}
		}
	}
}