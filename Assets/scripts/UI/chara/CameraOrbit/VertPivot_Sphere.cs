﻿using UnityEngine;
using System.Collections;

public class VertPivot_Sphere : MonoBehaviour {

	private float holdTimer = 0f;

	private float yaxis;
	private float xAxis;

	private float vertSpeed;
	private float horiSpeed;

	private float rotationY = 0F;
	private Quaternion originalRotation;
	//private float originalXRot;
	//private float originalYRot;

	public float rotationSpeed;

	public float lerpSpeed;

	public float minimumTilt = -30F;
	public float maximumTilt = 30F;


	void Start () {
		originalRotation = transform.localRotation;
		//originalXRot = transform.localEulerAngles.x;
		//originalYRot = transform.localEulerAngles.y;
	}

	void Update () {
		if(Input.GetMouseButton(0)) holdTimer++;

		if(Input.GetMouseButton(0) && holdTimer > 3f) {
			yaxis = -Input.GetAxis("Mouse Y");
			vertSpeed = yaxis;
			xAxis = Input.GetAxis("Mouse X");
			horiSpeed = xAxis;
			
		}
		else 
		{
			var ix = Time.deltaTime * lerpSpeed;
			vertSpeed = Mathf.Lerp(vertSpeed, 0, ix);
			horiSpeed = Mathf.Lerp(horiSpeed, 0, ix);
		}
		if(Input.GetMouseButtonUp(0)) {
			holdTimer = 0;
		}
		
		rotationY += vertSpeed * -rotationSpeed * 0.8f;
		rotationY = ClampAngle (rotationY, -maximumTilt, -minimumTilt);
		var yQuaternion = Quaternion.AngleAxis (rotationY, Vector3.left);
		transform.localRotation = originalRotation * yQuaternion;
		
		transform.Rotate(0, horiSpeed * rotationSpeed,0,  Space.World);
	}

	static float ClampAngle (float angle,float min,float max) {
		if (angle < -360)
			angle += 360;
		if (angle > 360)
			angle -= 360;
		return Mathf.Clamp (angle, min, max);
	}
}
