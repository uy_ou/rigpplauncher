﻿using UnityEngine;
using System.Collections;

public class PinchZoom : MonoBehaviour {

	private float minPinchSpeed = 5.0F;
	private float minDistance = 5.0F;
	private float touchDelta;
	private Vector2 previousDistance;
	private Vector2 currentDistance;
	private float speedTouch0;
	private float speedTouch1;

	public int speed = 3;
	public int maxOut = 40;
	public int maxIn = 15;

	Camera cam;

	void Start(){
		cam = this.GetComponent<Camera>();
	}
	
	void Update () {
		if (Input.touchCount == 2 && Input.GetTouch(0).phase == TouchPhase.Moved && Input.GetTouch(1).phase == TouchPhase.Moved) 
		{
			currentDistance = Input.GetTouch(0).position - Input.GetTouch(1).position;
			previousDistance = ((Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition) - (Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition));
			touchDelta = currentDistance.magnitude - previousDistance.magnitude;
			speedTouch0 = Input.GetTouch(0).deltaPosition.magnitude / Input.GetTouch(0).deltaTime;
			speedTouch1 = Input.GetTouch(1).deltaPosition.magnitude / Input.GetTouch(1).deltaTime;
			
			if ((touchDelta + minDistance <= 5) && (speedTouch0 > minPinchSpeed) && (speedTouch1 > minPinchSpeed))
			{
				cam.fieldOfView = Mathf.Clamp(cam.fieldOfView + (1 * speed),maxIn,maxOut);
			}
			
			if ((touchDelta + minDistance > 5) && (speedTouch0 > minPinchSpeed) && (speedTouch1 > minPinchSpeed))
			{
				cam.fieldOfView = Mathf.Clamp(cam.fieldOfView - (1 * speed),maxIn,maxOut);
			}
		}

		if( Input.GetAxis("Mouse ScrollWheel") < 0) {
			cam.fieldOfView = Mathf.Clamp(cam.fieldOfView + (1 * speed),maxIn,maxOut);
			
		} else if( Input.GetAxis("Mouse ScrollWheel") > 0){
			cam.fieldOfView = Mathf.Clamp(cam.fieldOfView - (1 * speed),maxIn,maxOut);
			
		}
	}
	
}
