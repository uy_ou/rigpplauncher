﻿using UnityEngine;
using System.Collections;

public class HoriPivot : MonoBehaviour {

	public float rotationSpeed;
	public float lerpSpeed;

	private float speed;
	private float holdTimer = 0;
	private float xAxis = 0;
	private int lastTouch = 0;
	
	void Start () {
	
	}
	

	void Update () {
		if(Input.touchCount < 2 && lastTouch < 2) {
			if(Input.touchCount == 0) lastTouch = 0;
			if(Input.GetMouseButton(0)) holdTimer++;
			if (Input.GetMouseButton(0) && holdTimer > 3)
			{
				holdTimer ++;
				xAxis = Input.GetAxis("Mouse X");
				speed = xAxis;
			} 
			else 
			{
				var i = Time.deltaTime * lerpSpeed;
				speed = Mathf.Lerp(speed, 0, i);
			}
			if (Input.GetMouseButtonUp(0))
			{
				holdTimer = 0;
			}
			transform.Rotate(0, speed * rotationSpeed,0,  Space.World);
		}
		else 
		{
			lastTouch = Input.touchCount;
			speed = 0;
		}
	}
}
