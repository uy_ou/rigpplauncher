﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class SelectWindow_Test : MonoBehaviour {

	public List<string> category = new List<string>();
	public GameObject toggle;
	public GameObject inputField;
	public Text title;
	

	void Start () {
		CreateToggleNode("Category",category);
	}

	void Update () {
	
	}

	public void CreateToggleNode(string name,List<string> myList){
		title.text = name;
		for(int i = 0; i < myList.Count; i++){
			GameObject newItem = Instantiate(toggle);
			newItem.name = "Toggle" + i.ToString();
			newItem.transform.GetChild(1).GetComponent<Text>().text = myList[i];
			newItem.transform.SetParent(inputField.transform);
		}
	}
}
