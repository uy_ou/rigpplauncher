﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MouseOverTrigger : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public Animator anim;

	public bool Activation;
	
	public void OnPointerEnter( PointerEventData eventData )
	{
		if(Activation){
			Debug.Log("Active!!");
			anim.SetTrigger("OnActive");
		}
	}
	
	public void OnPointerExit( PointerEventData eventData )
	{
		if(!Activation){
			Debug.Log("DeActive");
			anim.SetTrigger("DeActive");
		}
	}
}
