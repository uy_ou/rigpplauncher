﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScreenShot : MonoBehaviour {
	
	Image image;

	void Start () {
		image = GetComponent<Image>();
	}


	public void ImageSet(Sprite sprite){
		image.sprite = sprite;
	}
}
