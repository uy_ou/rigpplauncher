﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ShowScreenShot : MonoBehaviour, IPointerDownHandler{

	public ScreenShot screen;
	Image image;

	void Start () {
		image = GetComponent<Image>();
	}

	public void OnPointerDown(PointerEventData data){
		screen.ImageSet(image.sprite);
	}
	
}
