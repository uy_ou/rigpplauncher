﻿using UnityEngine;
using System.Collections;
using RiG_GameLanucher.Model;
using RiG_GameLanucher.UI.Panel;

namespace RiG_GameLanucher.UI.ToolTip
{
	public class ShowTooltip : MonoBehaviour {

		public GameObject toolTipObj;
		ToolTip tooltip;                  
		//public GameObject tooltipGameObject;                                      
		public GameInfo info;
		
		
		void Start()
		{
			//item = GetComponent<ItemOnObject>().item;
			//
			tooltip = toolTipObj.GetComponent<ToolTip>();
		}
		
		public void activateTooltip()
		{                                    
			//_inventory.activateTooltip(item); 
			// 
			
			if (tooltip != null)
			{
				toolTipObj.SetActive(true);
				GamePanel panel = transform.gameObject.GetComponent<GamePanel>();
				tooltip.activateTooltip(panel.gameInfo);
			}
			
		}

		
	}
}