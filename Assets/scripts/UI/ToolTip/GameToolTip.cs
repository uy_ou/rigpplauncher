using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using RiG_GameLanucher.Model;

namespace RiG_GameLanucher.UI.ToolTip
{
	public class GameToolTip : ToolTip {

		// ToolTip 
		// protected static Content contentinfo;
		//protected static Content contentInfo;

		//GUI
		[SerializeField]
		public static Image tooltipBackground;
		[SerializeField]
		public static Text tooltipTitle;
		[SerializeField]
		public static Text tooltipDesc;
		
		//Tooltip Settings
		[SerializeField]
		public static bool showTooltip;
		[SerializeField]
		private static bool showTooltipName;
		
		//Tooltip Objects
		[SerializeField]
		private static GameObject tooltip;
		[SerializeField]
		private static RectTransform tooltipRectTransform;
		[SerializeField]
		public static GameObject tooltipTextName;
		[SerializeField]
		public static GameObject tooltipTextDesc;
		[SerializeField]
		public static GameObject tooltipImageIcon;

		// Use this for initialization
		void Start () {
			init();
		}

		protected override void init(){
			base.init();
			Debug.Log("GameToolTip");
		}

		
		// Update is called once per frame
		void Update () {
		
		}

		// public override void setContent(){

		// }

		[SerializeField]
		public void StartGame(){
			if(contentInfo==null){
				Debug.Log("ぬるぽ");
				return;
			}
			System.Diagnostics.Process p = System.Diagnostics.Process.Start(contentInfo.FilePath);
			Debug.Log("ゲームスタート");
		}	
	}
}