﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using RiG_GameLanucher.Model;
using System; //Exception
using System.IO;
using System.Text;

namespace RiG_GameLanucher.UI.ToolTip
{
	public class ToolTip : MonoBehaviour {

		protected static Content contentInfo;

		//GUI
		[SerializeField]
		public Image tooltipBackground;
		[SerializeField]
		public Text tooltipTitle;
		[SerializeField]
		public Text tooltipDesc;
		
		//Tooltip Settings
		[SerializeField]
		public bool showTooltip;
		[SerializeField]
		private bool showTooltipName;
		
		//Tooltip Objects
		[SerializeField]
		private GameObject tooltip;
		[SerializeField]
		private RectTransform tooltipRectTransform;
		[SerializeField]
		public GameObject tooltipTextName;
		[SerializeField]
		public GameObject tooltipTextDesc;
		[SerializeField]
		public GameObject tooltipImageIcon;

		//private string guitxt = "";
		
		void Start()
		{
			deactivateTooltip();
		}

		protected virtual void init(){

		}

		public void activateTooltip<T>(T info) where T : Content
		{
			//gameObject.SetActive(true);
			//tooltipTextName.SetActive(true);
			//tooltipImageIcon.SetActive(true);
			//tooltipTextDesc.SetActive(true);
			//transform.GetChild(0).gameObject.SetActive(true);
			contentInfo = info;
			tooltipTitle.GetComponent<Text>().text = info.Name;
			tooltipDesc.GetComponent<Text>().text = ReadTextData (info);;
		}
		
		public void deactivateTooltip()             //deactivating the tooltip after you went out of a slot
		{
			//tooltipTextName.SetActive(false);
			//tooltipImageIcon.SetActive(false);
			//tooltipTextDesc.SetActive(false);
			//transform.GetChild(0).gameObject.SetActive(false);
		}

		string ReadTextData<T>(T info) where T : Content
		{

			FileInfo fi = new FileInfo(info.Readme);

			//read
			StreamReader sr = new StreamReader(fi.OpenRead());
			return sr.ReadToEnd();
			//while( sr.Peek() != -1 ){
				//return sr.ReadLine(); 
			//}
			sr.Close();

		}
	}
}